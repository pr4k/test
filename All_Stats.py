#!/usr/bin/env python
# coding: utf-8

# In[1]:


#!pip install --upgrade git+https://github.com/flairNLP/flair.git
import flair


# In[2]:


import pandas as pd
import numpy as np
import gensim
import numpy as np


# In[3]:


flair_sentiment = flair.models.TextClassifier.load('en-sentiment')


# In[4]:


def get_sentiment_from_review(review):
  try:
    s = flair.data.Sentence(review)
    flair_sentiment.predict(s)
    total_sentiment = s.labels
    return str(total_sentiment[0]).split()[0]
  except Exception as e:
    print(e)
    return ""


# In[5]:


import nltk
nltk.download('punkt')


# In[6]:



import pymongo
def upload_data_dict_to_mongo( name, data):   
  uri = "mongodb://polynomialai:e1NQdWv3X4qMdB5pguUaRVR40uz4f3GcsnDMNsGaDeTdegSnvNAvwEZSSpMZHXjfyF4yUzRkZQxYizTHzgKzUQ==@polynomialai.mongo.cosmos.azure.com:10255/?ssl=true&retrywrites=false&replicaSet=globaldb&maxIdleTimeMS=120000&appName=@polynomialai@"         
  client = pymongo.MongoClient(uri)
  mydb = client['quiltResult']     
  null = None
  col = mydb[name]                              
  col.collection.insert_many(data)   


# In[7]:


import nltk
import gender_ai as g
from nltk.tokenize import word_tokenize, sent_tokenize

def get_sentiment(dflist):

  final_list = []
  positive = 0
  negative = 0
  for row in dflist:
    try:
      review = row
      tokens = sent_tokenize(review)
      reviewsentiment = get_sentiment_from_review(review)
      if reviewsentiment == "POSITIVE":
        positive+=1
      else:
        negative+=1
    except:
      pass
    
  return {'postive':positive, 'negative':negative }
#upload_data_dict_to_mongo("SentientGender", final_list)


# In[8]:


import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
from guess_indian_gender import IndianGenderPredictor

def get_gender(df):
  df = df[['Author']]
  dflist = df.values.tolist()
  final_list = []
  male = 0
  g = IndianGenderPredictor()
  female = 0
  for row in dflist:
    try:
      author = row
      gender = g.predict(name = str(author))
      if gender == 'male':
        male+=1
      else:
        female+=1
    except:
      pass
  return {"male":male, "female": female}
#upload_data_dict_to_mongo("SentientGender", final_list)


# In[9]:


def upload_data_to_mongo(client):
    sheets = pd.read_excel("top10.xlsx", sheet_name = None)
    null = None
    Nan = None
    for name, sheet in sheets.items():
        try:
            data = eval(sheet.to_json(orient = 'records'))
            mydb = client['quiltResult']
            null = None
            col = mydb[name]
            col.collection.insert_many(data)
        except Exception as e:
            print(e)

def upload_data_dict_to_mongo(client, name, data):
    mydb = client['quiltResult']
    null = None
    col = mydb[name]
    col.collection.insert_many(data)
def retrieve_from_mongo(client, collection, query):
    mydb = client['quiltResult']
    col = mydb[collection]
    null = None
    NaN = None
    return list(col.find(query))


# In[10]:


import yake
def get_keywords(review_list):
  try:
    language = "en"
    deduplication_thresold = 0.9
    deduplication_algo = 'seqm'
    windowSize = 1
    numOfKeywords = 25
    max_ngram_size = 5
    custom_kw_extractor = yake.KeywordExtractor(lan=language, n=max_ngram_size, dedupLim=deduplication_thresold, dedupFunc=deduplication_algo, windowsSize=windowSize, top=numOfKeywords, features=None)
    response = []
    keywords_list = []
    keyphrases = []
    for product in review_list:
        try:
           
            product_name , review = product["ProductName"], product["ReviewText"]

            keywords = custom_kw_extractor.extract_keywords(str(review))
            temp = {
                    'ProductName':product_name.replace(".", " "),
                    'Keywords': [],
                    'KeyPhrases': []
                    }
            flag = 0
            for i in keywords:

                if len(i[1].split()) >1:
                  if flag ==0:
                    keyphrases.append(i[1].replace(".", " "))
                  flag =1
                else:
                  keywords_list.append(i[1].replace(".", " "))
        except Exception as e:
            print(e)
    from collections import Counter
    count = Counter(keywords_list)
    
    count = sorted(count.items(), key = lambda x:x[1], reverse = True)
    print(count[0:50])
    return {"keywords":{i[0]:i[1] for i in count[0:50]} , "keyphrases":keyphrases} 
  except Exception as e:
    print(e)
    return [e]
  


# In[11]:


def extract_keywords(df):

    df = df[["ProductName", "ReviewText"]]
    data = eval(df.to_json(orient = 'records'))
    data = get_keywords(data)
    return data

def get_volume_per_time(df):
    columns = ["ProductName", 'ReviewCount', "SentimentByRating",  "Sentiment", "ReviewDate", "FrequencyOfReviewPerDate", "Gender"]
    products = list(set(df['ProductName'].to_list()))
    final_list = []
    final_data = {}
    final_data['averageSentiment'] = round(df['ReviewRating'].mean(), 2)
    for product in products:
      try:
        final = {}
        final.setdefault('ProductName', [])
        ndf = df[df['ProductName'] == product]
        final.setdefault('SentimentByRating', [])
        countdf = ndf.groupby(ndf['Date']).count().reset_index()
        countdf = countdf[['Date', 'ReviewCount']]
        final['ProductName'] = product
        final['SentimentByRating'] = round(ndf['ReviewRating'].mean(), 2)
        final_list.append(final)
      except:
        pass
    final_data['productwiseSentiment'] = final_list
    return final_data

def rate_of_change(df):
    import datetime as dt
    columns = ["ProductName", 'ReviewCount', "SentimentByRating",  "Sentiment", "ReviewDate", "FrequencyOfReviewPerDate", "Gender"]
    products = list(set(df['ProductName'].to_list()))
    final_list = []
    ndf = df["Date"].value_counts(dropna = False).reset_index()
    ndf['NewDate'] = pd.to_datetime(ndf['index'], errors = 'coerce')
    ndf = ndf.sort_values(by="NewDate")
    ndf['index'] = ndf["NewDate"].dt.strftime('%Y-%m')
    ndf = ndf['index'].value_counts(dropna = False).reset_index()
    ndf['level_0'] = pd.to_datetime(ndf['level_0'], errors = 'coerce')
    ndf['Date'] = ndf["level_0"]
    
    ndf["count"] = ndf["index"]
    ndf = ndf[["Date", "count"]]
    ndf = ndf.sort_values(by="Date")
    ndf["rate_change"] = ndf['count'].pct_change()
    null = None
    ndf['Date'] = ndf['Date'].dt.strftime('%Y-%m')
    data = eval(ndf.to_json(orient = "records"))
    temp = {}
    print(data)

    return data

def get_stats_no(df):    
    dataset = df['ProductName'].value_counts(dropna = False).to_dict()
    temp = []
    index = df.index
    for i in dataset:
      try:
        temp.append({i.replace(".", " "):dataset[i]})
      except:
        pass
    return {"totalReviews":len(index), "productwiseCount":temp}
    


# In[12]:


def upload_data_to_mongo(client):
    sheets = pd.read_excel("top10.xlsx", sheet_name = None)
    null = None
    Nan = None
    for name, sheet in sheets.items():
        try:
            data = eval(sheet.to_json(orient = 'records'))
            mydb = client['quiltResult']
            null = None
            col = mydb[name]
            col.collection.insert_many(data)
        except Exception as e:
            print(e)


# In[13]:



def upload_one_to_mongo(client, name, data):
    mydb = client['quiltResult']
    null = None
    col = mydb[name]
    col.collection.insert_one(data)


# In[18]:


import threading

def main(): 
    df1 = pd.read_csv("80k.csv")
    df3 = pd.read_json("80k-3.json", lines=True)
    df4 = pd.read_json("80k-4.json", lines=True)
    df = pd.concat([df1, df3, df4], axis=0, sort=False)
    print(df["Brand"])
    df = df.sample(frac=1).reset_index(drop=True)
    brands = set(df["Brand"].to_list())
    
    print(len(df.index))
    threads = []
    
    for brand in brands:
        
        branddf = df[df['Brand']== brand]
        x = threading.Thread(target=execute, args=(brand, branddf,))
        threads.append(x)
        x.start()
    for index, thread in enumerate(threads):
        print("Main    : before joining thread %d.", index)
        thread.join()
        print("Main    : thread %d done", index)

        
        
def execute(brand, branddf):
        structure = {}
        index = 1
        categories = {}
        uri = "mongodb://polynomialai:e1NQdWv3X4qMdB5pguUaRVR40uz4f3GcsnDMNsGaDeTdegSnvNAvwEZSSpMZHXjfyF4yUzRkZQxYizTHzgKzUQ==@polynomialai.mongo.cosmos.azure.com:10255/?ssl=true&retrywrites=false&replicaSet=globaldb&maxIdleTimeMS=120000&appName=@polynomialai@"
        client = pymongo.MongoClient(uri)
        cat6 = 0
        cat5 = 0
        cat4 = 0
        cat3 = 0
        cat2 = 0
        cat1 = 0
        cindex = 0
        import math
        categories.setdefault("category6", [])
        categories.setdefault("category5", [])
        categories.setdefault("category4", [])
        categories.setdefault("category3", [])
        categories.setdefault("category2", [])
        categories.setdefault("category1", [])
        print(brand)
        c6 = True
        c5 = True
        c4 = True
        c3 = True
        c2 = True
        c1 = True
        try:  
          category6 = branddf["Category6"].to_list()[0]
        except:
          c6 = False
        try:
          category5 = branddf["Category5"].to_list()[0]
        except:
          c5 = False
        try:
          category4 = branddf["Category4"].to_list()[0]
        except:
          c4 = False
        try:
          category3 = branddf["Category3"].to_list()[0]
        except:
          c3 = False
        try:
          category2 = branddf["Category2"].to_list()[0]
        except:
          c2 = False
        try:
          category1 = branddf["Category1"].to_list()[0]
        except Exception as e:
          c1 = False
        try:
          c6 = not math.isnan(category6) 
          c5 = not math.isnan(category5) 
          c4 = not math.isnan(category4) 
          c3 = not math.isnan(category3) 
          c2 = not math.isnan(category2) 
          c1 = not math.isnan(category1) 
        except:
          pass

        print(c1, c2, c3, c4, c5, c6)
        for i in categories["category6"]:
          cindex = index
          if not c6:
            break
          if category6 in i:
            i[category6].append({"brand":index, "category_name" : category6 })
            break
        else:
          if c6:
            categories["category6"].append({category6:[{brand:index, "category_name" : category6 }], "_id": cat6, "next":True})
            cindex = cat6
            cat6+=1

        for i in categories["category5"]:
          if not c5:
            break
          if category5 in i:
            i[category5].append({"category6":cindex, "category_name" : category5})
            break
        else:
          if c5:
            categories["category5"].append({category5:[{"category6":cindex, "category_name" : category5 }], "_id": cat5, "next":c6})
            cindex = cat5
            cat5+=1

        for i in categories["category4"]:
          if not c4:
            break
          if category4 in i:
            i[category4].append({"category5":cindex, "category_name" : category4})
            break
        else:
          if c4:
            categories["category4"].append({category4:[{"category5":cindex, "category_name" : category4 }], "_id": cat4, "next":c5})
            cindex = cat4
            cat4+=1
          
        for i in categories["category3"]:
          if not c3:
            break
          if category3 in i:
            i[category3].append({"category4":cindex, "category_name" : category3})
            break
        else:
          if c3:
            categories["category3"].append({category3:[{"category4":cindex, "category_name" : category3 }], "_id": cat3, "next":c4})
            cindex = cat3
            cat3+=1

        for i in categories["category2"]:
          if not c2:
            break
          if category2 in i:
            i[category2].append({"category3":cindex, "category_name" : category2})
            break
        else:
          if c2:
            categories["category2"].append({category2:[{"category3":cindex, "category_name" : category2 }], "_id": cat2, "next":c3})
            cindex = cat2
            cat2+=1

        for i in categories["category1"]:
          if not c1:
            break
          if category1 in i:
            i[category1].append({"category2":cindex, "category_name" : category1})
            break
        else:
          if c3:
            categories["category1"].append({category1:[{"category2":cindex, "category_name" : category1 }], "_id": cat1, "next":c2})
            cindex = cat1
            cat1+=1
        
        
        print(categories)
        

        reviewtext = branddf["ReviewText"]
        structure['Brand'] = brand
        structure['_id'] = index
        structure["keywords"] = extract_keywords(branddf)
        print("Keywords completed")

        structure["rate_of_change"] = rate_of_change(branddf)
        print("rate change completed")
      
        structure['number_of_review'] = get_stats_no(branddf)
        print("no of review completed")
        structure['averageSentiment'] = get_volume_per_time(branddf)
        print("sentiment average completed")
        #structure['Sentiment'] = get_sentiment(reviewtext)
        #print("flair sentiment completed")
        structure['gender'] = get_gender(branddf)
        sheets = pd.read_excel("top10.xlsx", sheet_name = None)
        null = None
        Nan = None
        
        
        for name, sheet in sheets.items():
            try:
                print(name)
                newdf = sheet[sheet["Brand"]==brand]
                data = eval(newdf.to_json(orient = 'records'))[0]
                print(name == "Age Distribution")
                if name == "Age Distribution":
                  print("YES")
                  totalMen = data["Men"]
                  totalWomen = data["Women"]
                  del data["Men"]
                  del data["Women"]
                  del data["Brand"]
                  data = {"ageGroupStats":data, "Brand": brand, "totalMen":totalMen, "totalWomen":totalWomen}
                  print(data)


               
     
                structure[name] = data
            except Exception as e:
                print(e)
                pass
    
          
        try:
          import json
          out_file = open("{}.json".format(brand), "w")  
      
          json.dump(structure, out_file, indent = 6) 
          out_file.close()
          #upload_one_to_mongo(client, 'testbrand2', structure)
          index+=1
        except Exception as err:
          print(structure)
          print(err.details)
        
        print(categories)
        '''
        for i in categories.keys():
          if len(categories[i])>0:
            data = categories[i]
            data = [str(i) for i in data]
            data = list(set(data))
            data = [eval(i) for i in data]
            upload_data_dict_to_mongo(client, i, data)
        '''
    
    


# In[19]:



main()


# In[ ]:




